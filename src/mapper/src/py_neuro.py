import sys, time
import numpy as np
import cv2
import roslib
import rospy
from sensor_msgs.msg import CompressedImage
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Int16MultiArray

class image_feature:

    def __init__(self):
        self.subscriber = rospy.Subscriber("/output/image_raw/compressed", CompressedImage, self.callback,  queue_size = 1)
        self.res_pub = rospy.Publisher("/output/neuro/cords", Int16MultiArray,  queue_size = 1)


    def callback(self, ros_data):
        np_arr = np.fromstring(ros_data.data, np.uint8)
        cv_image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        
        #cv2.imshow("Neuro window", cv_image)
        cv2.waitKey(3)
        msg = Int16MultiArray()
        msg.data = [20, 10]
        rospy.loginfo(msg)
        self.res_pub.publish(msg)


def main(args):
    ic = image_feature()
    rospy.init_node('neuro')
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)