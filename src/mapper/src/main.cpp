
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <librealsense2/rs.hpp>
#include <librealsense2/rsutil.h>
#include <opencv2/opencv.hpp>
#include <sstream> 

using namespace cv;    

const String DEB_STAT_OK = "[ OK ] ";
const String DEB_STAT_NOK = "[ ! ] ";  

int main(int argc, char * argv[]) try
{
    ros::init(argc, argv, "realsense_capture");
    ros::NodeHandle nHandler;
    image_transport::ImageTransport it(nHandler);
    image_transport::Publisher pub = it.advertise("camera/image", 1);
    sensor_msgs::ImagePtr msg;

    rs2::colorizer color_map;
    rs2::pipeline pipe;  

    if(pipe.start())
    {
        std::cout << DEB_STAT_OK << "Started pipe\n";
    }
    
    auto color_window_name = "Color Frame";
    auto depth_window_name = "Color Frame";

    namedWindow("Color Frame", WINDOW_AUTOSIZE);
    namedWindow("Depth Frame", WINDOW_AUTOSIZE);

    ros::Rate loop_rate(1);
    ros::ServiceServer service = n.advertiseService("get_depth", get);

    
    while (waitKey(1) < 0 && cvGetWindowHandle(color_window_name) && nHandler.ok())
    {
        rs2::frameset data = pipe.wait_for_frames(); // Wait for next set of frames from the camera
        
        rs2::frame depth = data.get_depth_frame();
        rs2::frame color = data.get_color_frame();
            
        // Frame size (width and height) for color stream
        const int cw = color.as<rs2::video_frame>().get_width();
        const int ch = color.as<rs2::video_frame>().get_height();
        
        // Frame size (width and height) for color stream
        const int dw = depth.as<rs2::video_frame>().get_width();
        const int dh = depth.as<rs2::video_frame>().get_height();
        
        rs2::depth_frame dpt_frame = depth.as<rs2::depth_frame>();
        float pixel_distance_in_meters = dpt_frame.get_distance(dw/2, dh/2);
        std::cout << pixel_distance_in_meters << "\n";

        // Create OpenCV matrix
        Mat color_img(Size(cw, ch), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP);
        cvtColor(color_img, color_img, CV_RGB2BGR);
        Mat depth_img(Size(dw, dh), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP);

        msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", color_img).toImageMsg();
        pub.publish(msg);
        // Show camera data
        imshow(color_window_name, color_img);
        imshow(depth_window_name, depth_img);
    }

    return EXIT_SUCCESS;
}
catch (const rs2::error & e)
{
    std::cerr << DEB_STAT_NOK << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception& e)
{
    std::cerr << DEB_STAT_NOK << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (cv_bridge::Exception& e)
{
    ROS_ERROR("Could not convert to bgr8");
    return EXIT_FAILURE;
}


