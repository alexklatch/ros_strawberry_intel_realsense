
import roslib
roslib.load_manifest('mapper')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Int16MultiArray

import threading

class image_converter:

  def __init__(self):
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("camera/image", Image,self.callback, queue_size = 1)
    self.image_pub = rospy.Publisher("/output/image_raw/compressed", CompressedImage, queue_size = 1)
    self.data_sub = rospy.Subscriber("/output/neuro/cords", Int16MultiArray, self.data_callback, queue_size = 1)
    self.subscriber = rospy.Subscriber("/camera/image/compressed", CompressedImage, self.img_callback,  queue_size = 1)
    self.img = None
    self.counter = 0

  def callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      self.img = cv_image
    except CvBridgeError as e:
      print(e)
     
    #cv2.imshow("Image window", cv_image)
    cv2.waitKey(3)

  def data_callback(self, data):
    print(data.data[0])
    print(data.data[1])

  def img_callback(self, ros_data):
    self.counter+=1
    if self.counter % 20 == 0:
      msg = CompressedImage()
      msg.header.stamp = rospy.Time.now()
      msg.format = "jpeg" 
      msg.data = np.array(cv2.imencode('.jpg', self.img)[1]).tostring()
      self.image_pub.publish(msg)       
    
def main(args):
  ic = image_converter()
  rospy.init_node('image_converter')

  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
